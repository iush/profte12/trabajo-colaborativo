# Sistemas para la enseñanza colaborativa guiada.  

Alcance

- Este projecto tiene un enfoque constructivista:
    - El concepto fundamental que permite la enseñanza colaborativa guiada es el del historico de trabajo revicion:
	- Es una grabacion de acciones que permite al estudiante recostruir los pasos posteriormente.

Poblacion:
- Estudiantes y/o Ingenirieros de sistemas:
    - OpenSource es compartir conocimiento
	- Es la forma mas pura de compartir conocimiento.
    - Por que los latinos no compartimos.
    - Compartir codigo publicamente es una ventaja competitiva.
- Orientado al aprendizage de desarrollo de software. 
- Solo se verificara la interface para conectar con:
    - Tecnologos.
    - Sociales.

Herramientas
- Blog:
    - 
- GitLab:
    - Mermaid
- Wiki
- Encuestas
- Micro Servicios (para cada asignatura)

Metodologia:
- Agilismo
- DevOps
- Micro-Servicios.

#### Retos tecnologicos derivados:

- Sincronizacion entre actores.

#### Puntos importantes:

- El aprendizaje con tutor es habitualmente un caso extremo de colaboracion dirigida.
- El aprendizaje en grupo entre estudiantes, suele ser de colaboracion independiente.
- El papel de profesor, deja de ser una simple fuente de conocimiento, convirtiendose en experto en aprendizaje.

#### Glosario

- CSCW:
    - Computer Supported Collaborative Work
- CSCL:
    - Computer Supported Collarative Learning

#### Links:

- https://www.youtube.com/watch?v=FsAOilrirvo
- https://sites.google.com/site/groupccygv/wiki-del-proyecto/1-las-organizaciones-como-generadoras-de-conocimiento/trabajo-colaborativo-y-aprendizaje-colaborativo-2
- https://www.semana.com/opinion/blogs/entrada-blog/cooperar-colaborar-cual-diferencia/321190
- https://www.teachingchannel.org/video/collaboration-vs-cooperative-learning-nea 
