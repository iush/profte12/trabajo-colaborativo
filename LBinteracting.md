## **Aprender interactuando**, "learning-by-interacting"

![Interactuando](images/3thinks.jpg)

>>>>

### Integracion Continua

- [Implementando un Laboratorio para CI en GitLab](http://arsoft12.gitlab.io/jekyll/hwarch/posts/software-architecture-main-doc/)
- [Laboratorio: Implementando un Laboratorio para CI en GitLab](http://arsoft12.gitlab.io/jekyll/hwarch/posts/implementando-ci/)

>>>>

### Automatic build:

[![AB](images/CI.png)](https://gitlab.com/iush/iush.gitlab.io/blob/master/.gitlab-ci.yml)

>>>>

### Pipeline

[![Pipeline](images/pipelines.png)](https://gitlab.com/iush/iush.gitlab.io/pipelines)

>>>>

### Chapters:

[![Chapters](images/chapters.png)](https://gitlab.com/iush/iush.gitlab.io/pipelines/36088315)

>>>>

### Output:

[![Chapters](images/out.png)](https://gitlab.com/iush/iush.gitlab.io/-/jobs/118769013)


>>>>
### GitLab para Instituciones Educativas.

[Education Program requirements](https://about.gitlab.com/solutions/education/)

>>>>

- [ ] Agregando a los usuarios en el grupo IUSH.
- [ ] Dando permisos a los usuarios con pleno control de su proyecto.

----
