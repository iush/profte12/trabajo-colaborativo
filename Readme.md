## Introducción

- Proceso en el que un individuo aprende más de lo que aprendería por sí solo.
- Basado en una fuerte relación de interdependencia entre los diferentes miembros del grupo.

----

