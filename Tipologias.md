## **Aprender haciendo**, "learning-by-doing"

[General document Software engineering](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/ingenieria-software-finalwork/)

>>>>

### Paradigma actual en el manejo actual de proyectos Universitarios.

[![Paradigm School](images/paradigm.png)](https://www.ijires.org/administrator/components/com_jresearch/files/publications/IJIRES_477_Final.pdf)

>>>>

### Es Microsoft Word la mejor herramienta para seguimiento de proyectos en Ingenieria de Sistemas?

![Word MEME](images/word-meme.jpg)

>>>>

###  Conjunto de Herramientas 

- Utilizar herramientas en donde sea posible, inclusive para actividades de documentacion.
[![GitLab](images/gitlab2devopts.png)](https://about.gitlab.com/devops-tools/)


>>>>

### Pero realmente Git es una tendencia?

[![trend](images/git_overtime.png)](https://trends.google.com/trends/explore?q=%2Fm%2F05vqwg,%2Fm%2F09d6g,%2Fm%2F012ct9,%2Fm%2F08441_&hl=en-US&tz=&tz=)

>>>>

### Diff Git vs TFS

![Git vs TFS](images/GITvsTFS.png)

>>>>

### Microsoft buy GitHub

![Microsoft](images/mocosoftgithub.png)

>>>>

### Repositorio

[![Repositorio](images/repository.png)](https://gitlab.com/iush/iush.gitlab.io/tree/master)

>>>>

### Markdown

[![Markdown](images/markdown.png)](https://gitlab.com/iush/profte12/trabajo-colaborativo/blob/master/Tipologias.md)

>>>>

### [Jekyll](https://jekyllrb.com/docs/)
[Powered by Ruby](https://www.ruby-lang.org/en/)

Jekyll is a simple, extendable, static site generator. You give it text written in your favorite markup language and it churns through layouts to create a static website.

![jekyll](images/jekyll.jpg)

>>>>

### [Revela.js](https://revealjs.com/#/)
[Powered by JS](https://www.javascript.com/)

![RevealJS](images/revealjs.jpg)
>>>>

### Issues

[![Issues](images/issues.png)](https://gitlab.com/iush/profte12/trabajo-colaborativo/issues)

>>>>

### Board ([kanban](https://en.wikipedia.org/wiki/Kanban) style)

[![Board](images/checklist.jpg)](https://gitlab.com/iush/profte12/trabajo-colaborativo/boards)

>>>>

### Aplicacion de ejemplo:

[![Aplication example](images/app.png)](http://arquh11.gitlab.io/jekyll/hwarch/posts/engineering-hardware-w/)

>>>>

### Integracion Continua

- [Implementando un Laboratorio para CI en GitLab](http://arsoft12.gitlab.io/jekyll/hwarch/posts/software-architecture-main-doc/)
- [Laboratorio: Implementando un Laboratorio para CI en GitLab](http://arsoft12.gitlab.io/jekyll/hwarch/posts/implementando-ci/)

>>>>

### Automatic build:

[![AB](images/CI.png)](https://gitlab.com/iush/iush.gitlab.io/blob/master/.gitlab-ci.yml)

>>>>

### Pipeline

[![Pipeline](images/pipelines.png)](https://gitlab.com/iush/iush.gitlab.io/pipelines)

>>>>

### Chapters:

[![Chapters](images/chapters.png)](https://gitlab.com/iush/iush.gitlab.io/pipelines/36088315)

>>>>

### Output:

[![Chapters](images/out.png)](https://gitlab.com/iush/iush.gitlab.io/-/jobs/118769013)


>>>>
### GitLab para Instituciones Educativas.

[Education Program requirements](https://about.gitlab.com/solutions/education/)

>>>>

### Crear usuario en GiLab

- [ ] Crear usuario en GitLab.

----

## **Aprender interactuando**, "learning-by-interacting"

- [ ] Agregando a los usuarios en el grupo IUSH.
- [ ] Dando permisos a los usuarios con pleno control de su proyecto. 

----

## **Aprender buscando**, "learning-by-searching"

----

## **Aprender compartiendo**, "learning-by-sharing"

----

## **Aprender siendo**, "learning-by-being"

>>>>

### Catalaxia

----
