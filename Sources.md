## Important links and sources:

- [Trabajo-colaborativo-y-aprendizaje-colaborativo.](https://sites.google.com/site/groupccygv/wiki-del-proyecto/1-las-organizaciones-como-generadoras-de-conocimiento/trabajo-colaborativo-y-aprendizaje-colaborativo-2)
>>>>

### Paradigma de aula

- [School on the cloud](https://www.ijires.org/administrator/components/com_jresearch/files/publications/IJIRES_477_Final.pdf)

>>>>

### Word Alternatives:

- [18 Software Documentation Tools](https://www.process.st/software-documentation/)
- [21 Software Documentation Tools](https://stepshot.net/21-software-documentation-tools-for-every-stage-of-project-implementation/)
- [How to document a Software Development Project](https://www.smartics.eu/confluence/display/PDAC1/How+to+document+a+Software+Development+Project)
- [5 open source alternatives to Google Docs](https://opensource.com/business/15/7/five-open-source-alternatives-google-docs)

>>>>

### Open source

- [8 ways to get started in opensource](https://www.youtube.com/watch?v=8e9QVNrAYpI)
- 

>>>>

### Git and GitLab

- [The case for Git in 2015](http://www.netinstructions.com/the-case-for-git/)
- [Razones para usar GIT antes que TFS](https://openwebinars.net/blog/razones-para-usar-git-antes-que-tfs/)

>>>>

## Catalaxia

- [Por qué STEAM es una REVOLUCIÓN dentro del capitalismo?](https://www.youtube.com/watch?v=I6WTyBjMfys)
- [Empresario de ti mismo](https://www.youtube.com/watch?v=0u9xBl0riy0)

>>>>

### Data Resources:

- [visor-anda](https://sitios.dane.gov.co/visor-anda/)

>>>>

### Ends
Tanks!
![Tool](images/owl.jpg "Process")

