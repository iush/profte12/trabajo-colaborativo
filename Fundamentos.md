## Fundamentos
![El ladrillo](images/14084405.jpg)

>>>>

### Trabajo colaborativo y aprendizaje colaborativo

El trabajo colaborativo, en un contexto educativo, constituye un modelo de aprendizaje interactivo, que invita a los estudiantes a construir juntos.

>>>>

### DIFERENCIA ENTRE COLABORACIÓN Y COOPERACIÓN
![Diferencia](images/img2.png)

>>>>

#### DIFERENCIA ENTRE COLABORACIÓN Y COOPERACIÓN
![Diferencia](images/table1.png)

>>>>

### Características

- Se basa en una fuerte relación de interdependencia entre los distintos miembros que componen el grupo, de manera que el alcance final de las metas concierna a todos los participantes.
- Hay una clara responsabilidad individual de cada miembro en la consecución de la meta final.
- La formación en habilidades o características de los miembros del grupo es heterogénea.

>>>>

#### Características

- Todos los integrantes tienen su parte de responsabilidad en la ejecución de las acciones del grupo.
- La responsabilidad de cada miembro del grupo es compartida.
- Se persigue el logro de objetivos a través de la realización individual y conjunta de tareas.

>>>>

#### Características

- Existe una interdependencia positiva entre los miembros del grupo.
- Se exigen habilidades comunicativas, relaciones simétricas y recíprocas, así como el deseo de compartir la resolución de las tareas.

>>>>

### Teorías sobre el aprendizaje

- Constructivismo.
- Conectivismo.
- Aprendizaje rizomático.
- Inteligencia conectiva.

>>>>

### Tipologías de aprendizaje:

**Aprender haciendo**, "learning-by-doing": se utilizan las herramientas que permiten al estudiante y/o docente la lectura y la escritura bajo el principio ensayo-error.

>>>>

#### Tipologías de aprendizaje:

**Aprender interactuando**, "learning-by-interacting": en las plataformas se intercambian ideas con el resto de usuarios de internet. El énfasis de aprender interactuando se pone en la comunicación entre iguales.

>>>>

#### Tipologías de aprendizaje:

**Aprender buscando**, "learning-by-searching": se trata de buscar las fuentes que ofrezcan información sobre el tema a tratar. Este proceso de investigación, selección y adaptación acaba ampliando y enriqueciendo el conocimiento de la persona que realiza la búsqueda.

>>>>

#### Tipologías de aprendizaje:

**Aprender compartiendo**, "learning-by-sharing": el intercambio de conocimientos y experiencias enriquece el proceso educativo. Las herramientas web 2.0 son la base para compartir información, conocimientos y experiencias.

>>>>

#### Tipologías de aprendizaje:

**Aprender siendo**, "learning-by-being": en las comunidades se aprende a aprender y a ser participantes legítimos. Se aprende a tomar compromisos con las prácticas y normas de una determinada profesión.

>>>>

### Transformaciones en el aprendizaje
![Transformaciones aprendizaje](images/uuuu.jpg)

>>>>

### Transformaciones en la enseñanza
![Transformaciones enseñanza](images/jjjj.jpg)

----
