## **Aprender compartiendo**, "learning-by-sharing"

>>>>

### [Holokit](https://holokit.io/)

[![Motivation](images/MotivationAccessibility.png)](https://www.youtube.com/watch?v=Wp21BynNl1k)

>>>>

### Raptor

[![Raptor](images/raptor.png)](https://www.youtube.com/watch?v=qWlCItmdYdY&t=0s&list=PLMWUQLTJLTIzZZKgbiB5lcdlB7ksVbvub)

>>>>

### [POWER UP](https://www.poweruptoys.com/)

[![PowerUP](images/powerup.png)](https://www.youtube.com/watch?v=q9bpp7zmM_A)

>>>>

### OpenHardware

[![Open Source Harware](images/OH-logo.png)](https://opensource.com/resources/what-open-hardware)

>>>>

### [OpenCPU](https://openrisc.io/)

- [Risk-V](https://www.datacenterknowledge.com/hardware/open-source-risc-v-ready-take-intel-amd-and-arm-data-center)
- [![OpenCPU](images/opencpu.png)](https://www.youtube.com/watch?v=L8jqGOgCy5M&list=LLn2ug_RrVv-Gdp6Mm-ejKkg&index=6&t=414s)

>>>>

### [Arduino](https://store.arduino.cc/usa/)

[![Arduino](images/ardLicence.png)](https://www.arduino.cc/en/Main/FAQ)

>>>>

### Adafruit

[![Adafruit](images/adafruit.png)](https://www.adafruit.com/product/693)

>>>>

### SparkFun

- [SuperAwesomeSylvia](https://www.youtube.com/channel/UCKUaaSvg7mLCU-b0IhJMz-Q)
- [![SparkFun](images/Spark.png)](https://www.sparkfun.com/news)

>>>>

### Es el final de Arduino?

[![FPGA](images/FPGA.png)](https://www.youtube.com/watch?v=RYAk5bZtj4U)
- Microntroladores y Microprocesadores.
- Aterrizar en la luna.

>>>>

### RepRap / Clone Wars

- Impresoras auto-replicables.
- [![Impresoars](images/impresoras.png)](https://www.youtube.com/watch?v=RYAk5bZtj4U)

>>>>

### Icestudio

[![icestudio](images/icestudio.png)](https://www.youtube.com/watch?v=RYAk5bZtj4U)

>>>>

### [FPGAwars](http://fpgawars.github.io/)

[![FPGAwars](images/fpga_logos.png)](http://fpgawars.github.io/)

>>>>

### ICEZUM Alhambra board

[![ICEZUM Alhambra board](images/icezum-alhambra-v1.1.jpg)](https://github.com/fpgawars/icezum/wiki)

>>>>

### [IceStudio](https://github.com/fpgawars/icestudio)

[![IceStudio](images/icestuido_cir.png)](https://www.youtube.com/watch?v=w7X_Ht0gfY0)

>>>>

### Arduino MKR VIDOR 4000

[![Vidor](images/vidor.png)](https://store.arduino.cc/usa/arduino-vidor-4000)

>>>>

### 

----
